class SionnaParams:
    """Represents Sionna input parameters"""

    def __init__(self, radio_name, height, lon, lat, freq, sim_type, antenna_model, sim_height, resolution, txpower, *args, **kwargs):
        """
        :param radio_name: basestation endpoint name e.g. "cbrssdr1-ustar-comp"
        """
        self.radio_name = radio_name
        self.height = height
        self.radio_lon = lon
        self.radio_lat = lat
        self.freq = freq
        self.sim_type = sim_type
        self.antenna_model = antenna_model
        self.sim_height = sim_height
        self.resolution = resolution
        self.txpower = txpower
        self.param_dict = {
            "radio_name": radio_name,
            "height": height,
            "radio_lon": lon,
            "radio_lat": lat,
            "freq": freq,
            "sim_type": sim_type,
            "antenna_model": antenna_model,
            "sim_height": sim_height,
            "resolution": resolution,
            "txpower": txpower,
        }

    def get_param_dict(self):
        return self.param_dict

    def __str__(self):
        return f'\n\
        RADIO NAME: {self.radio_name}\n\
        TX HEIGHT(m): {self.height}\n\
        RADIO LONGITUDE: {self.radio_lon}\n\
        RADIO LATITUDE: {self.radio_lat}\n\
        FREQUENCY(MHz): {self.freq}\n\
        SIM TYPE: {self.sim_type}\n\
        ANTENNA MODEL: {self.antenna_model}\n\
        SIM HEIGHT: {self.sim_height}\n\
        RESOLUTION: {self.resolution}\n\
        TXPOWER: {self.txpower}\n'

    def __unicode__(self):
        s = f'\n\
        RADIO NAME: {self.radio_name}\n\
        TX HEIGHT(m): {self.height}\n\
        RADIO LONGITUDE: {self.radio_lon}\n\
        RADIO LATITUDE: {self.radio_lat}\n\
        FREQUENCY(MHz): {self.freq}\n\
        SIM TYPE: {self.sim_type}\n\
        ANTENNA MODEL: {self.antenna_model}\n\
        SIM HEIGHT: {self.sim_height}\n\
        RESOLUTION: {self.resolution}\n\
        TXPOWER: {self.txpower}\n'
        return s.encode('utf-8')
