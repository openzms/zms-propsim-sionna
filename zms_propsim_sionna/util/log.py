import logging

LOGGING_HANDLER = None

def configure_logging(debug=False):
    global LOGGING_HANDLER
  
    if LOGGING_HANDLER is None:
        formatter = logging.Formatter("%(asctime)s - %(name)-12s - %(levelname)-8s - %(message)s")
        console = logging.StreamHandler()
        console.setFormatter(formatter)
        logging.getLogger().addHandler(console)
        LOGGING_HANDLER = console
  
    if debug:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.WARN)
    return

def configure_file_logging(logger_name, log_file, level=logging.DEBUG):
    l = logging.getLogger(logger_name)
    #formatter = logging.Formatter('%(asctime)s : %(message)s')
    formatter = logging.Formatter('%(created)f - %(message)s')

    fileHandler = logging.FileHandler(log_file, mode='a')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    # Add the following line if want to add console logging
    #l.addHandler(streamHandler) 
