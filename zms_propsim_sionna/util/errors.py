
class PropSimSionnaError(Exception):
    pass

class ConfigError(PropSimSionnaError):
    pass

class JobExistsError(PropSimSionnaError):
    pass

class InvalidJobError(PropSimSionnaError):
    pass

class InvalidJobOutputError(PropSimSionnaError):
    pass

class JobParameterError(PropSimSionnaError):
    pass
