#!/usr/bin/env python3

import setuptools

if __name__ == "__main__":
    setuptools.setup(
        name="zms-propsim-sionna",
        version="0.1.0",
        author="David M. Johnson, Serhat Tadik",
        author_email="",
        maintainer="David M. Johnson",
        maintainer_email="johnsond@flux.utah.edu",
        url="https://gitlab.flux.utah.edu/openzms/zms-propsim-sionna",
        description="A Sionna-based propagation service for OpenZMS.",
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Environment :: Other Environment",
            "Intended Audience :: Developers",
            "Operating System :: OS Independent",
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Topic :: Utilities",
        ],
        packages=setuptools.find_packages(),
        install_requires=[
            "zms-api @ git+https://gitlab.flux.utah.edu/openzms/zms-api.git@43f6474#subdirectory=python",
        ],
        python_requires=">=3.7",
        setup_requires=[
            "setuptools",
        ],
    )
